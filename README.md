# Praktikum Sistem Multimedia

# No 1
###### https://github.com/dzakifikri/Flask-RemovalWatermarkBlueRed
# No 2
###### Program yang saya buat adalah program untuk menghapus watermark yang dibatasi untuk warna biru dan merah saja, dan juga setelah image processing akan langsung mengkompresi image tersebut yang akan disimpan dalam format JPEG. Penggunaan cv2.imwrite('hasil.jpg', resized_res) menyimpan gambar hasil dalam format JPEG (jpg). JPEG adalah format kompresi gambar yang umum digunakan yang dapat mengurangi ukuran file dengan mengorbankan sebagian kualitas gambar. Proses kompresi ini terjadi saat Anda menyimpan gambar dengan format JPEG.

##### Library yang digunakan :
##### 1. OpenCV -> Library ini digunakan untuk pengolahan gambar dan komputer visi
##### 2. NumPy -> Digunakan bekerja dengan aray dalam konteks pengolahan gambar.
##### 3. Flask -> Flask disini sebagai framework yang berfungsi untuk mengatur rute dan menangani permintaan HTTP dari pengguna.
##### 4. Werkzeug -> Fungsi ini digunakan untuk mengamankan nama file yang diunggah. Dalam kode ini, secure_filename digunakan untuk mendapatkan nama file yang aman sebelum menyimpannya.

###### Algoritma program Image :

```mermaid
graph TD;
A[Start] --> B[Render Template index.html]
B --> C[Upload File]
C --> D[Save File]
D --> E[Remove Watermark]
E --> F[Save Result]
F --> G[Download Result]
G --> H[End]
C --> H
```

###### Algoritma Proses Code Image :

```mermaid
graph TD;
A[Start] --> B[Menampilkan halaman utama]
B --> C[Tombol unggah file ditekan]
C --> D[Memeriksa file yang diunggah]
D --> E[Menyimpan file yang diunggah]
E --> F[Panggil fungsi remove_watermark]
F --> G[Menghapus watermark pada gambar]
G --> H[Menyimpan hasil penghapusan watermark]
H --> I[Mengunduh hasil penghapusan watermark]
I --> J[Selesai]
C --> B
```

###### Berikut demo programnya :
![demo-image](https://gitlab.com/1207050031/praktikum-sistem-multimedia/-/raw/main/Image.gif)
###### Link Youtube : https://youtu.be/srFWdsM4mss

# No 3
###### Aspek kecerdasan yang digunakan dalam program ini adalah pengolahan citra. Proses penghapusan watermark mengandalkan teknik pengolahan citra, yang melibatkan penggunaan algoritma dan metode matematika untuk memanipulasi citra digital. Dalam konteks projek penghapusan watermark, teknik pengolahan citra yang digunakan mungkin termasuk deteksi warna, segmentasi, dan operasi bitwise.

# No 4
###### https://github.com/dzakifikri/Flask-AudioTranscibeFromWAV
# No 5
###### Program yang dibuat menggunakan bahasa pemrograman Python adalah sebuah aplikasi speech-to-text. Tujuan utama dari program ini adalah untuk mengambil suara dari file audio dengan ekstensi WAV dan mengubahnya menjadi teks yang dapat dibaca. Sebelum proses pengenalan suara dilakukan, program akan melakukan kompresi pada file audio untuk mengurangi ukurannya menggunakan pydub dengan menggunakan birate 64k. Audio yang sudah dikompresi kemudian dibaca dan direkam menggunakan SpeechRecognition seperti sebelumnya. Selanjutnya, audio yang sudah dikompresi digunakan untuk proses pengenalan suara dan hasil transkripsi ditampilkan kepada pengguna.

##### Library yang digunakan :
##### 1. Flask ->  Framework web yang digunakan untuk membangun aplikasi web. (Menyediakan fitur dasar untuk mengatur rute dan menangani permintaan HTTP.)
##### 2. Pydub -> Library ini digunakan untuk manipulasi audio. (Digunakan untuk mengompresi file audio yang diunggah oleh pengguna.)
##### 3. io -> Modul ini digunakan untuk mengelola operasi input/output. (Digunakan untuk membaca dan menulis data audio menggunakan io.BytesIO.)
##### 4. SpeechRecognition -> SpeechRecognition adalah sebuah library Python yang digunakan untuk mengenali dan memproses teks dari suara atau ucapan manusia.

###### Algoritma program Audio :
```mermaid
graph TD;
A[Start] --> B[Render Template index.html]
B --> C[Receive File Upload]
C --> D[Check File]
D --> E[Compress Audio]
E --> F[Extract Compressed Audio]
F --> G[Speech Recognition]
G --> H[Transcribe Speech]
H --> I[Render Template with Transcript]
I --> J[End]
```

###### Berikut demo programnya :
![demo-image](https://gitlab.com/1207050031/praktikum-sistem-multimedia/-/raw/main/Audio.gif)
###### Link Youtube : https://youtu.be/791pMvVlZHU
# No 6
###### Penggunaan aspek kecerdasan buatan adalah penggunaan pustaka SpeechRecognition memanfaatkan kecerdasan buatan untuk melakukan pengenalan suara. Pustaka ini memungkinkan program untuk mengambil input suara dari pengguna melalui file audio yang diunggah, kemudian menggunakan model pengenalan suara yang telah dilatih untuk mengubah suara menjadi teks. Kedua, pustaka pydub digunakan untuk mengompresi audio yang diunggah agar memenuhi kebutuhan format dan bitrate yang diinginkan, sehingga penggunaan kecerdasan buatan ini memungkinkan pemrosesan audio dengan efisien.
